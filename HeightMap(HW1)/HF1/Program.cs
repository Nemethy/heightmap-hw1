﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HF1
{
    class HeightMap
    {
        short[,] elevations;
        const int SIZEX = 1201;
        const int SIZEY = 1201;
        int et;
        public int ElevationThreshold {
            get { return et; }
            set {
                if (value < 0 || value > short.MaxValue)
                    throw new ArgumentOutOfRangeException("Incorrect number!");
                else et = value;
            }
        }

        public HeightMap()
        {
            elevations = new short[SIZEX, SIZEY];
        }

        public static HeightMap Parse(string path)
        {
            HeightMap hmap = new HeightMap();
            FileStream fs = new FileStream(path, FileMode.Open);
            //using (System.IO.BinaryReader br = new System.IO.BinaryReader(System.IO.File.Open(path, System.IO.FileMode.Open,)))
            using (BinaryReader br = new BinaryReader(fs))
            {
                for (int i = 0; i < SIZEX; i++)
                    for (int j = 0; j < SIZEY; j++)
                        hmap.elevations[i, j] = (short)((br.ReadByte() << 8) | br.ReadByte());
            }
            fs.Close();

            return hmap;
        }

        private short[] GetExtremalElevations()
        {
            short[] minMax = new short[] {short.MaxValue, short.MinValue};
            
            foreach(short i in elevations)
            {
                if (minMax[0] > i && i != -32768) minMax[0] = i;
                if (minMax[1] < i) minMax[1] = i;
            }

            return minMax;
        }

        public void SaveToBitmap(string path)
        {
            Bitmap bmp = new Bitmap(SIZEX, SIZEY);
            Color c;
            short[] minMax = GetExtremalElevations();
            int extent = minMax[1] - minMax[0];

            for (int i = 0; i < SIZEX; i++)
                for (int j = 0; j < SIZEY; j++)
                {
                    if (elevations[i, j] > 32767 || elevations[i, j] == -32768) c = Color.FromArgb(255, 0, 0); // 32768
                    else if (elevations[i, j] < ElevationThreshold) c = Color.DodgerBlue;
                    else c = Color.FromArgb(0, 
                        (byte)(255 - (double)((elevations[i, j] - minMax[0]) * 255) / extent), 
                        0);

                    bmp.SetPixel(i, j, c);
                    Console.Clear();
                    Console.WriteLine("Under processing...");
                    Console.WriteLine("{0:F2}%", (double)((i * SIZEY + j) * 100) / (SIZEX * SIZEY));
                    // Thread.Sleep(10);
                }

            bmp.Save(path.Contains(".bmp") ? String.Concat(path, "") : String.Concat(path, ".bmp"));
        }
    }

    class Program
    {
        static void Main(string[] args) // pl. myApp.exe N46E017.hgt test.bmp 120
        {
             try
             {
                 HeightMap hmap = HeightMap.Parse(args[0]);
                 hmap.ElevationThreshold = Convert.ToInt32(args[2]);
                 hmap.SaveToBitmap(args[1]);
             }
             catch (ArgumentException e)
             {
                 Console.WriteLine(e.Message);
                 if(e is ArgumentOutOfRangeException)
                     Console.WriteLine("integer -> [0, 32767]");
                 Console.ReadLine();
                 return;
             }
             catch (FormatException)
             {
                 Console.WriteLine("Try with an integer value from the interval [0, 32767]");
                 Console.ReadLine();
                 return;
             }
             catch (FileNotFoundException)
             {
                 Console.WriteLine("The file doesn't exist!");
                 Console.ReadLine();
                 return;
             }

            Console.WriteLine("Completed!");
            Console.ReadLine();
        }
    }
}
